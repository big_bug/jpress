package io.jpress.web.front;

import com.jfinal.aop.Before;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.web.base.ControllerBase;
import io.jpress.web.base.UserInterceptor;

/**
 * @author Michael Yang 杨福海 （fuhai999@gmail.com）
 * @version V1.0
 * @Package io.jpress.web
 */
@RequestMapping("/ucenter")
@Before({UserInterceptor.class, UserCenterInterceptor.class})
public class UserCenterController extends ControllerBase {


    @Override
    public void render(String view) {
        super.render("/WEB-INF/views/ucenter/" + view);
    }


    /**
     * 用户中心首页
     */
    public void index() {
        render("index.html");
    }


    public void info() {
        render("info.html");
    }

    /**
     * 个人签名
     */
    public void signature() {
        render("signature.html");
    }


    /**
     * 头像设置
     */
    public void avatar() {
        render("avatar.html");
    }

    /**
     * 账号密码
     */
    public void pwd() {
        render("pwd.html");
    }


}
